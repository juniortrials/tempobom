import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity
} from "react-native";

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TextInput>cidade</TextInput>
        <TouchableOpacity>
          <Text>Qual a temperatura ? </Text>
        </TouchableOpacity>
        <Text style={styles.numTemp}>32 graus</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  numTemp: {
    fontSize: 30
  }
});
